package com.nmo.data.taxonomy;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.List;

/**
 * Created by Peter on 2016-04-17.
 */
@SolrDocument
@Data
public class Taxonomy {

    @Id
    @Field
    private String id;

    @Field
    private String name;

    @Field("parent")
    private String parent;

    @Field("children")
    private List<String> children;

}
