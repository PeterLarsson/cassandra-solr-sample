package com.nmo.data.taxonomy;

import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

/**
 * Created by Peter on 2016-04-17.
 */
public interface TaxonomyRepository extends SolrCrudRepository<Taxonomy, String> {
    List<Taxonomy> findByNameStartingWith(String name);
}
