package com.nmo.data;

/**
 * Created by Peter on 2016-04-18.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nmo.data.category.Category;
import com.nmo.data.category.CategoryRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
public class MessageConsumer {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CategoryRepository categoryRepository;

    // TBD: to be removed, currently (mis)used to simplify waiting process in test.
    private CountDownLatch latch = new CountDownLatch(1);

    @SneakyThrows
    public void handleMessage(final String message) {
        log.info("handleMessage <{}>", message);
        Category category = objectMapper.readValue(message, Category.class);
        categoryRepository.save(category);
        latch.countDown();
        latch = new CountDownLatch(1);
   }

    public CountDownLatch getLatch() {
        return latch;
    }

}
