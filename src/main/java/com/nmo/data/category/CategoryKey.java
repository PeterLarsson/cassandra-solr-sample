package com.nmo.data.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

import java.io.Serializable;

/**
 * Created by Peter on 2016-04-17.
 */
@PrimaryKeyClass
@Data
@AllArgsConstructor(staticName = "of")
public class CategoryKey implements Serializable {
    public CategoryKey() {}

    @PrimaryKeyColumn(ordinal = 1)
    private String country;
    @PrimaryKeyColumn(ordinal = 2)
    private String brand;
    @PrimaryKeyColumn(ordinal = 3)
    private String channel;
    @PrimaryKeyColumn(ordinal = 4, type = PrimaryKeyType.PARTITIONED)
    private String categoryId;
}