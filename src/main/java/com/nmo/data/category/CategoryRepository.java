package com.nmo.data.category;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Peter on 2016-04-17.
 */
public interface  CategoryRepository extends CrudRepository<Category, CategoryKey> {
    @Query("Select * from category where categoryId=?0")
    Category findByCategoryId(String categoryId);
}
