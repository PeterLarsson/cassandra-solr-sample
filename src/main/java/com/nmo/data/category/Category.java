package com.nmo.data.category;

import lombok.Data;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * Created by Peter on 2016-04-17.
 */
@Table
@Data
public class Category {

    @PrimaryKey
    private CategoryKey categoryKey;
    private String parentId;
    private String name;
}
