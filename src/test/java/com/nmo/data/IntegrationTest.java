package com.nmo.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nmo.data.category.Category;
import com.nmo.data.category.CategoryKey;
import com.nmo.data.category.CategoryRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Peter on 2016-04-18.
 */
@Slf4j
public class IntegrationTest extends TestSupport {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    MessageConsumer messageConsumer;

    @Autowired
    CategoryRepository categoryRepository;

    @Value("${category.exchange.name}")
    String exchangeName;

    @Value("${spring.rabbitmq.port}")
    int port;

    @Test
    @SneakyThrows
    public void sendCategoryMessage() {
        Category category = new Category();
        CategoryKey categoryKey = CategoryKey.of("SE", "NM", "WEB", "CAT0001");
        category.setCategoryKey(categoryKey);
        category.setName("TEST");

        log.info("sendCategoryMessage: {}", category);
        sendMessage(objectMapper.writeValueAsString(category));

        if (!messageConsumer.getLatch().await(2000, TimeUnit.MILLISECONDS)) {
            throw new TimeoutException();
        }

        Category processedCategory = categoryRepository.findOne(categoryKey);

        assertEquals(category, processedCategory);

    }


    public void sendMessage(String message) {
        rabbitTemplate.convertAndSend(exchangeName, "#", message);
    }
}
