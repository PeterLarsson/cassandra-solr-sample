package com.nmo.data.category;

import com.nmo.data.TestSupport;
import org.cassandraunit.spring.CassandraUnitDependencyInjectionTestExecutionListener;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;

import static org.junit.Assert.assertEquals;

/**
 * Created by Peter on 2016-04-17.
 */
public class CategoryTest extends TestSupport {

    @Autowired
    CategoryRepository categoryRepository;

    public static class OrderedCassandraTestExecutionListener
            extends CassandraUnitDependencyInjectionTestExecutionListener {
        @Override
        public int getOrder() {
            return Ordered.HIGHEST_PRECEDENCE;
        }
    }

    @Before
    public void before() {
        categoryRepository.deleteAll();
    }

    @Test
    public void createCategoryTest() {
        Category category = category("cat-1", "test");
        Category saved = categoryRepository.save(category);
        assertEquals(category, saved);
    }

    @Test
    public void findByCategoryKeyTest() {
        Category category = category("cat-1", "test");
        Category saved = categoryRepository.save(category);
        Category retrieved = categoryRepository.findOne(category.getCategoryKey());
        assertEquals(saved, retrieved);
    }

    Category category(String categoryId, String name) {
        Category category = new Category();
        CategoryKey categoryKey = CategoryKey.of("US", "NM", "WEB", categoryId);
        category.setCategoryKey(categoryKey);
        category.setName(name);
        return category;
    }
}
