package com.nmo.data;

import com.google.common.io.Files;
import lombok.SneakyThrows;
import org.apache.qpid.server.Broker;
import org.apache.qpid.server.BrokerOptions;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;

/**
 * Created by Peter on 2016-04-18.
 */
@Configuration
public class TestBrokerConfiguration {

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    Queue queue;

    @Autowired
    TopicExchange topicExchange;

    @Autowired
    Binding binding;

    @Value("${spring.rabbitmq.port}")
    int port;

    @Bean
    @SneakyThrows
    Broker amqpBroker() {
        final Broker broker = new Broker();

        BrokerOptions brokerOptions = new BrokerOptions();
        brokerOptions.setConfigProperty("qpid.amqp_port", String.valueOf(port));
        brokerOptions.setConfigProperty("qpid.work_dir", Files.createTempDir().getAbsolutePath());
        brokerOptions.setConfigProperty("qpid.pass_file",
                applicationContext.getResource("broker-pwd.properties").getFile().getAbsolutePath()
        );
        brokerOptions.setInitialConfigurationLocation(
                applicationContext.getResource("classpath:broker.json").getURL().toExternalForm()
        );

        broker.startup(brokerOptions);

        final CachingConnectionFactory cf = new CachingConnectionFactory(port);
        final RabbitAdmin admin = new RabbitAdmin(cf);
        admin.declareQueue(queue);
        admin.declareExchange(topicExchange);
        admin.declareBinding(binding);
        cf.destroy();

        return broker;
    }
}
