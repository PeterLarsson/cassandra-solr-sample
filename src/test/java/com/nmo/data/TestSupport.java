package com.nmo.data;

import com.nmo.data.category.CategoryTest;
import org.cassandraunit.spring.CassandraDataSet;
import org.cassandraunit.spring.EmbeddedCassandra;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Peter on 2016-04-17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@TestExecutionListeners(mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS, listeners = {
        CategoryTest.OrderedCassandraTestExecutionListener.class })
@IntegrationTest("spring.data.cassandra.port=9142")
@CassandraDataSet(keyspace = "category", value = "setup.cql")
@EmbeddedCassandra(timeout = 60000)
public abstract class TestSupport {
}
