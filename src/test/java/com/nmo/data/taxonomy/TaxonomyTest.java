package com.nmo.data.taxonomy;

import com.nmo.data.TestSupport;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Peter on 2016-04-17.
 */
@EnableSolrRepositories(repositoryBaseClass = TaxonomyRepository.class)
public class TaxonomyTest extends TestSupport {

    @Autowired
    TaxonomyRepository taxonomyRepository;

    @Test
    public void before() {
        taxonomyRepository.deleteAll();
    }

    @Test
    public void createTaxonomyTest() {
        Taxonomy taxonomy = taxonomy("1", "test");
        Taxonomy saved = taxonomyRepository.save(taxonomy);
        assertEquals(taxonomy, saved);
    }

    @Test
    public void findByNameTest() {
        Taxonomy taxonomy = taxonomy("1", "test");
        Taxonomy saved = taxonomyRepository.save(taxonomy);
        List<Taxonomy> retrieved = taxonomyRepository.findByNameStartingWith("te");
        assertEquals(saved, retrieved.get(0));
    }

    Taxonomy taxonomy(String id, String name) {
        Taxonomy taxonomy = new Taxonomy();
        taxonomy.setId(id);
        taxonomy.setName(name);
        return taxonomy;
    }

}
