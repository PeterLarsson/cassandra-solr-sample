package com.nmo.data.taxonomy;

import lombok.SneakyThrows;
import org.apache.solr.client.solrj.SolrServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.server.support.EmbeddedSolrServerFactory;

/**
 * Created by Peter on 2016-04-17.
 */
@Configuration
public class TaxonomyTestConfiguration {

    @Bean(name = "solrServer")
    @SneakyThrows
    public SolrServer solrServer() {
        EmbeddedSolrServerFactory factory = new EmbeddedSolrServerFactory("classpath:solr");
        return factory.getSolrServer();
    }

    @Bean
    public SolrOperations solrTemplate() {
        return new SolrTemplate(solrServer());
    }

}
